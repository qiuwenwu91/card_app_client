import App from './App';
import plugin from '@/plugins/index.js';
import { createPinia } from 'pinia';

// #ifndef VUE3
import Vue from 'vue';
import store from "./store"; // 添加全局存储vuex引入
import './uni.promisify.adaptor';

Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
	...App
})
app.use(plugin).use(store).use(createPinia()).$mount()
// #endif

// #ifdef VUE3
import {
	createSSRApp
} from 'vue'
import store from "./store"; // 添加全局存储vuex引入
export function createApp() {
	const app = createSSRApp(App).use(plugin).use(store).use(createPinia())
	return {
		app
	}
}
// #endif