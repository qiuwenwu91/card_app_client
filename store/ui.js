import {
	ref,
	computed
} from 'vue';
import {
	defineStore
} from 'pinia';

import UI from '@/core/ui';
var ui = new UI();

export const uiStore = defineStore('ui', () => {
	return ui
});