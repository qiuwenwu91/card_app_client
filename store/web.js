import {
	ref,
	computed
} from 'vue';
import {
	defineStore
} from 'pinia';

export const webStore = defineStore('web', () => {
	var active_index = ref(1);
	var select_icon = ref(0);
	var conf = ref({
		wechat_appid: "",
		wechat_mini_appid: "",
		web_ico: "/img/logo.png",
		web_as: "",
		web_title: 'AI大门，一触即开，未来已来！',
		wechat_mini_adid: "",
		register_mode: 0,
		wechat_mini_login: false,
		default_banner: "/img/banner.png",
		activation_gain_day: 1,
		activation_gain_limit: 10000,
		mini_app_menu: true,
		app_pay: false,
		service_qq: "",
		service_email: "",
		service_phone: "",
		service_wechat: "",
		service_wechat_qrcode: ""
	});
	var info = ref({
		project_id: 0,
		lang_type: uni.db.get('lang_type') || 'en',
		auth: [],
		ads: []
	});


	return {
		active_index,
		select_icon,
		conf,
		info,

		set_auth(_auth) {
			info.value.auth.clear()
			info.value.auth.addList(_auth);
		},

		set_lang(_lang) {
			info.value.lang_type = _lang;
			uni.db.set('lang_type', _lang);
		},

		set_conf(_config) {
			for (var k in _config) {
				conf.value[k] = _config[k]
			}
		},

		set_ad(_ads) {
			info.value.ads.clear()
			info.value.ads.addList(_ads);
		}
	}
});