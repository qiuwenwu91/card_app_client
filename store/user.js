import {
	ref,
	computed
} from 'vue';
import {
	defineStore
} from 'pinia';

import User from '@/core/user';
var user = new User();

export const userStore = defineStore('user', () => {
	const info = ref(user.info);

	return {
		info,
		set(_info) {
			for (var k in _info) {
				info.value[k] = _info[k];
			}
			if (_info.token) {
				uni.db.set("token", _info.token);
			}
		},
		sign_out() {
			uni.clear(info.value);
			// info.token = null;
			// info.username = null;
			var path = location.pathname;
			var url = "/";
			uni.db.del("token");
		},
		add(k, num) {
			info.value[k] += num;
		}
	}
});