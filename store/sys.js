export default {
	state() {
		var text = uni.getStorageSync('lang_type');
		var lang_type = 'en_US';
		if (text) {
			var json = JSON.parse(text);
			lang_type = json.value;
		}
		return {
			lang_type,
			config: {}
		}
	},
	mutations: {
		set_lang(state, lang_type) {
			state.lang_type = lang_type;
		},
		set_config(state, config) {
			$.push(state.config, config, true);
		}
	}
}