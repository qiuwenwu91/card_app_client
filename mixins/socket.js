export default {
	data() {
		return {
			is_use_ws: false,
			ws_config: {
				url: this.$host_ws("{host}/ws/comment", "ws"),
				name: "comment"
			},
			arr_msg: []
		}
	},
	methods: {
		/**
		 * 接收消息函数
		 * @param {String} message
		 */
		receive: function(data) {
			// console.log('data', typeof(data), data)
			try {
				if (data.result) {
					data = data.result
				} else if (data.content || Array.isArray(data)) {
					this.arr_msg.push(data);
				} else {
					this.noticy('message', data);
				}
			} catch (e) {
				console.error(e);
			}
		},
		/**
		 * 连接通知
		 * @param {String} type 通知类型
		 * @param {Object} content 通知内容
		 */
		noticy: function(type, content) {
			console.log(type, content, typeof(content));
		},
		/**
		 * 创建连接
		 */
		connect: function() {
			// console.log('建立连接');
			var url = this.ws_config.url + "?diy=" + Math.ceil(Math.random() * 100);
			this.ws = uni.socket(url, this.receive, this.noticy, this.ws_config.name);
			this.set_method();
		},
		/**
		 * 发送消息
		 * @param {String} content 
		 */
		send: function(msg) {
			msg.user_id = this.user.info.user_id;
			this.ws.req('message', msg);
		},
		set_method: function(methods = {}) {
			var lt = this.arr_msg;
			// 除了主动请求外, 还可以添加方法, 让服务端直接访问
			this.ws.methods.get_message = function(params) {
				var len = lt.length;
				return lt[len - 1];
			}
			uni.push(this.ws.methods, methods, true);
		}
	},
	onLoad() {
		if (this.is_use_ws) {
			this.connect()
		}
	},
	onUnload() {
		if (this.ws) {
			this.ws.close()
		}
	}
}
