/**
 * 面板类
 */
class Panel {
	/**
	 * 构造函数
	 * @param {Object} config
	 */
	constructor(config) {
		this.name = "";	// 名称，用于识别组件
		this.title = "";	// 标题，用于显示给用户看
		this.width = "calc(100vw - 2rem)";	// 宽度
		this.height = "calc(100vh - 2rem)";	// 高度
		this.position = "fixed";	// 定位方式
		this.pos_x = "50%";	// 横坐标
		this.pos_y = "50%";	// 纵坐标
		this.style = "transform: translate(-50%, -50%);";	// 附加样式
		this.z_index = 100;
		
		this.buttons = [];	// 按钮组
		this.list = [];	// 列表，用于物品
		this.tabs = [];	// 选项卡
	}
}

/**
 * 初始化函数
 * @param {Object} config 配置参数
 */
Panel.prototype.init = function(config) {
	Object.assign(this, config);
}

/**
 * 监听事件
 * @param {Object} ename
 * @param {Object} func
 */
Dialog.prototype.on = function(ename, func) {
	switch (ename){
		case "click":
			break;
		case "selected":
			break;
		case "hover":
			break;
		default:
			break;
	}
}

export default Panel