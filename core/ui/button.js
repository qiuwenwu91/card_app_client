/**
 * 按钮
 */
class Button {
	/**
	 * 构造函数
	 * @param {Object} config
	 */
	constructor(config) {
		this.name = "";	// 名称，用于识别组件
		this.title = "";	// 标题，用于显示给用户看
		this.width = "fit-content";	// 宽度
		this.height = "fit-content";	// 高度
		this.position = "relative";	// 定位方式
		this.pos_x = "";	// 横坐标
		this.pos_y = "";	// 纵坐标
		this.style = "";	// 附加样式
		this.z_index = 1;	// 图层
		
		this.text = "";	// 按钮显示内容
		
		this.state_default = {};	// 默认状态效果
		this.state_selected = {};	// 选中状态效果
		this.state_hover = {};	// 经过状态效果
		this.state_click = {};	// 点击效果
	}
}

/**
 * 初始化函数
 * @param {Object} config 配置参数
 */
Button.prototype.init = function(config) {
	Object.assign(this, config);
}

/**
 * 监听事件
 * @param {Object} ename
 * @param {Object} func
 */
Dialog.prototype.on = function(ename, func) {
	switch (ename){
		case "click":
			break;
		case "selected":
		case "hover":
			break;
		default:
			break;
	}
}

export default Button;