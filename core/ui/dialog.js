
import Avatar from './avatar.js';

/**
 * 对话框类
 */
class Dialog {
	/**
	 * 构造函数
	 * @param {Object} config
	 */
	constructor(config) {
		this.name = "";	// 名称，用于识别组件
		this.title = "";	// 标题，用于显示给用户看
		this.width = "fit-content";	// 宽度
		this.height = "fit-content";	// 高度
		this.position = "fixed";	// 定位方式
		this.pos_x = "50%";	// 横坐标
		this.pos_y = "50%";	// 纵坐标
		this.style = "transform: translate(-50%, -50%);";	// 附加样式
		this.z_index = 1000;
		
		this.buttons = [];	// 按钮组
		this.avatar = new Avatar();
	}
}

/**
 * 初始化函数
 * @param {Object} config 配置参数
 */
Dialog.prototype.init = function(config) {
	Object.assign(this, config);
}

export default Dialog