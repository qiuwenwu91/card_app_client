class UI {
	/**
	 * 构造函数
	 * @param {Object} config
	 */
	constructor(config) {
		this.name = "";	// 名称，用于识别组件
		this.title = "";	// 标题，用于显示给用户看
		this.width = "0";	// 宽度
		this.height = "0";	// 高度
		this.position = "fixed";	// 定位方式
		this.pos_x = "50%";	// 横坐标
		this.pos_y = "50%";	// 纵坐标
		this.style = "transform: translate(-50%, -50%);";	// 附加样式
		this.z_index = 10;
	}
}

/**
 * 初始化函数
 * @param {Object} config 配置参数
 */
UI.prototype.init = function(config) {
	Object.assign(this, config);
}

export default UI;