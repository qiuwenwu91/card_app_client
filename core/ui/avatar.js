/**
 * 头像
 */
class Avatar {
	/**
	 * 构造函数
	 * @param {Object} config
	 */
	constructor(config) {
		this.name = "";	// 名称，用于识别组件
		this.title = "";	// 标题，用于显示给用户看
		this.width = "fit-content";	// 宽度
		this.height = "fit-content";	// 高度
		this.position = "absolute";	// 定位方式
		this.pos_x = "0";	// 横坐标
		this.pos_y = "0";	// 纵坐标
		this.style = "";	// 附加样式
		this.z_index = 1;	// 图层
		
		this.image = "";	// 图片
		
	}
}

/**
 * 初始化函数
 * @param {Object} config 配置参数
 */
Avatar.prototype.init = function(config) {
	Object.assign(this, config);
}

export default Avatar