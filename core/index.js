import Player from './user/player.js';
import User from './user/index.js';

/**
 * 游戏引擎核心
 */
class Core {
	/**
	 * 构造函数
	 * @param {Object} config 配置参数
	 */
	constructor(config) {
		this.config = {};

		/**
		 * 游戏数据
		 */
		this.monsters = []; // 怪物信息
		this.items = []; // 物品信息

		/**
		 * 主要元素
		 */
		this.list_ui = []; // 操作界面
		this.list_scene = []; // 场景
		this.list_panel = []; // 面板，如：角色属性面板、装备面板、物品面板、好友面板、排行榜
		this.list_dialog = []; // 对话框，确认框、上下聊天框、任务接受/拒绝框、确认/取消框、警告框

		/**
		 * 操作索引
		 */
		this.index_ui = -1; // 操作界面索引，-1则不启用操作界面，例如：登录页，游戏结束页，无需操作界面
		this.index_scene = -1; // 场景索引，-1则不加载场景
		this.index_panel = -1; // 面板索引，-1则不弹出面板
		this.index_dialog = -1; // 弹窗索引，-1则不显示其他窗口，注：不建议游戏同时出现多个弹窗，需要两个窗口可由1个面板+1个弹窗完成

		this.init(config);
	}
}

/**
 * 初始化函数
 * @param {Object} config 配置参数
 */
Core.prototype.init = function(config) {
	Object.assign(this.config, config);
	this.user = new User();
	this.player = new Player();
}

export default Core