/**
 * 元素
 */
class Element {
	/**
	 * 构造函数
	 * @param {Object} config
	 */
	constructor(config) {
		this.name = ""; // 名称
		this.width = "fit-content"; // 宽度
		this.height = "fit-content"; // 高度
		this.position = "absolute"; // 定位方式
		this.pos_x = ""; // 横坐标
		this.pos_y = ""; // 纵坐标
		this.style = ""; // 附加样式
		
		this.image = ""; // 图片
	}
}

/**
 * 初始化函数
 * @param {Object} config 配置参数
 */
Element.prototype.init = function(config) {
	Object.assign(this, config);
}

export default Element;