import Maps from './maps.js';
import BG from './bg.js';
import Entry from './entry.js';

/**
 * 场景类
 */
class Scene {
	/**
	 * 构造函数
	 * @param {Object} config
	 */
	constructor(config) {
		// 名称
		this.name = "";
		
		this.maps = new Maps();
		this.bg = new BG();
		this.entry = new Entry();
	}
}

/**
 * 初始化函数
 * @param {Object} config 配置参数
 */
Scene.prototype.init = function(config) {
	Object.assign(this, config);
}

export default Scene