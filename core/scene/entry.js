
/**
 * 参与层类
 */
class Entry {
	/**
	 * 构造函数
	 * @param {Object} config
	 */
	constructor(config) {
		// 名称
		this.name = "";
		this.list = [];
	}
}

/**
 * 初始化函数
 * @param {Object} config 配置参数
 */
Entry.prototype.init = function(config) {
	Object.assign(this, config);
}

export default Entry