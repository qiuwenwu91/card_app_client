/**
 * 背景类
 */
class BG {
	/**
	 * 构造函数
	 * @param {Object} config
	 */
	constructor(config) {
		// 名称
		this.name = "";

		this.bgm = "/static/audio/music_main.mp3";
		this.count = 48;
		this.format = "/static/src/bg/dreamland/dreamland_{0}.jpg";
		this.width = 618;
		this.height = 325;
		this.row = 8;
		this.col = 6;

		this.list = [];
		this.image = {
			src: ""
		};
	}
}

/**
 * 初始化函数
 * @param {Object} config 配置参数
 */
BG.prototype.init = function(config) {
	Object.assign(this, config);
}

export default BG