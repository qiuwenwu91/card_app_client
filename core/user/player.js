/**
 * 玩家类
 */
class Player {
	/**
	 * 构造函数
	 * @param {Object} config
	 */
	constructor(config) {
		// 名称
		this.name = "";
		// 状态, 用于切换对话框内容
		this.status = 0;
		// 转变，用于切换窗口
		this.turn = 1;
		// 战斗中, 是否处于战斗状态，决定一些道具、技能能否使用
		this.fighting = 0;
	}
}

/**
 * 初始化函数
 * @param {Object} config 配置参数
 */
Player.prototype.init = function(config) {
	Object.assign(this, config);
}

export default Player