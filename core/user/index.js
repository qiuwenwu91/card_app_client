class User {
	/**
	 * 用户
	 * @param {Object} config 配置信息
	 */
	constructor(config) {
		this.info = {
			token: "",
			avatar: "",
			username: "",
			nickname: "",
			coin: 18830,
			diamond: 1288,
			voucher: 308,
			// 用户地址
			address: "",
			// USDT币
			usdt: 0,
			// 分红
			stock: 0,
			// VIP等级
			vip: 0,
			// VIP鉴定卡
			icard: 0,
			// 盲盒
			blind_box: 0
		}
	}
}

export default User;