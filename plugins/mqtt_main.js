import Mqtt from '../utils/mm_mqtt.js';

var token_now = "";
export default {
	install(Vue, options) {
		Vue.prototype.$sendSync = function(content, type = "text") {
			var {
				token,
				nickname
			} = this.$store.state.user;
			
			var msg = {
				token,
				from: nickname,
				to: '系统',
				type,
				content
			}
			var _this = this;
			this.$store.commit('add_msg', msg);
			return new Promise(function(resolve, reject) {
				try {
					_this.$mqtt.req('$SYS/game/server', 'game_msg', msg, (json) => {
						resolve(json);
					});
				} catch (e) {
					reject(e);
					resolve(null);
				}
			});
		}

		// 增加mqtt通讯器
		Vue.prototype.$mqtt = new Mqtt();

	}
}