/**
 * @fileOverview 用户定义vue全局函数, 均以$前缀命名, 代表全局函数
 * @author <a href="http://www.fman.top">自由人网络</a>
 * @version 1.0
 */
var host = "https://www.yxnav.com/";
var host_ws = "wss://www.yxnav.com/";
var appid = "wxe5647dd9c7442a26";

export default {
	/**
	 * 安装
	 * @param {Object} Vue vue类
	 * @param {Object} optons 配置参数
	 */
	install(Vue, optons) {
		if (optons && optons.host) {
			host = optons.host;
		}

		Vue.config.globalProperties.$appid = function(value) {
			if (value) {
				appid = value;
			}
			return appid;
		}

		var redirect_url = "";
		Vue.config.globalProperties.$host = function(url) {
			var ht = host.trim("/");
			return url ? url.replace("{host}", ht) : ht;
		};

		Vue.config.globalProperties.$host_ws = function(url) {
			var ht = host_ws.trim("/");
			return url ? url.replace("{host}", ht) : ht;
		};

		Vue.config.globalProperties.$copy = function(value) {
			uni.setClipboardData({
				data: value, //要被复制的内容
				success: () => { // 复制成功的回调函数
					this.$toast('复制成功')
				}
			})
		};

		/**
		 * 转为键值
		 * @param {Object} arr 数组
		 * @param {String} key 键
		 * @param {String} name 名称
		 * @param {String} value 默认值
		 */
		Vue.config.globalProperties.$to_kv = function(arr, key, name, value) {
			if (value === undefined) {
				value = '';
			}
			var list = [];
			if (arr.length > 0) {
				if (key) {
					var n = name ? name : 'name';
					for (var i = 0; i < arr.length; i++) {
						var o = arr[i];
						list.push({
							name: o[n],
							value: o[key]
						});
					}
					if (arr[0].name !== '') {
						list.unshift({
							name: '',
							value: value
						});
					} else {
						list[0].value = value;
					}
				} else if (arr.length && typeof(arr[0]) === "object") {
					list = arr;
					if (arr[0].name !== '') {
						list.unshift({
							name: '',
							value: value
						});
					}
				} else {
					for (var i = 0; i < arr.length; i++) {
						var o = arr[i];
						list.push({
							name: o,
							value: i
						})
					}
					if (arr[0] !== '') {
						list.unshift({
							name: '',
							value: value
						});
					} else {
						list[0].value = value;
					}
				}
			}
			return list;
		};

		/**
		 * 转换时间
		 * @param {String} timeStr 时间字符串
		 * @param {String} format 转换格式
		 * @return {String} 返回转换后的结果
		 */
		Vue.config.globalProperties.$to_time = function(timeStr, format) {
			if (!timeStr) {
				return ""
			}
			var time = timeStr.toTime();
			if (format) {
				return time.toStr(format);
			} else {
				var date = time.toStr("yyyy-MM-dd");
				var now = new Date();
				if (date == now.toStr("yyyy-MM-dd")) {
					return time.toStr("hh:mm")
				} else if (date == now.addDays(-1).toStr("yyyy-MM-dd")) {
					return "昨天" + time.toStr("hh:mm")
				} else if (time.toStr("yyyy") == now.toStr("yyyy")) {
					return time.toStr("MM-dd");
				} else {
					return date;
				}
			}
		};


		// 开始
		/**
		 * @description 提示框
		 * @param {String} text 提示内容
		 * @param {String} type 显示类型
		 */
		Vue.config.globalProperties.$toast = function(text, type = 'dark') {
			uni.showToast({
				title: text,
				icon: "none",
				duration: 2000
			});
		};

		/**
		 * @description 确认框
		 * @param {String} text 提示内容
		 * @param {Function} func 回调函数
		 * @param {String} title 标题
		 */
		Vue.config.globalProperties.$confirm = function(content, func, title = "操作提醒") {
			uni.showModal({
				title,
				content,
				// cancelText: "取消", // 取消按钮的文字  
				// confirmText: "确认", // 确认按钮的文字
				// showCancel: true,
				// data: {
				// 	modalShow: false
				// },
				// confirmColor: '#f55850',
				// cancelColor: '#39B54A',
				success: function(res) {
					if (res.confirm) {
						if (func) {
							func(true);
						}
					}
				}
			});
		};

		/**
		 * @description 过滤数组
		 * @param {Array} arr 被过滤的数组
		 * @param {String} key 判断的键
		 * @param {Object} value 判断的值
		 * @return {Array} 返回过滤后的数组
		 */
		Vue.config.globalProperties.$filter = function(arr, key, value) {
			var ar = [];
			for (var i = 0; i < arr.length; i++) {
				var o = arr[i];
				if (o[key] === value) {
					ar.push(o);
				}
			}
			return ar;
		};

		/**
		 *  改变对象属性时间
		 * @param {Object} o 被改变的对象
		 */
		Vue.config.globalProperties.$changeTime = function(o) {
			for (var k in o) {
				if (k.indexOf('time') !== -1) {
					if (typeof(k) == 'string') {
						var val = o[k];
						if (val || val.indexOf('T') !== -1) {
							var v = new Date(o[k]);
							o[k] = v.toStr('yyyy-MM-dd hh:mm:ss');
						} else if (/\d+/.test(val)) {
							if (o[k].length == 10) {
								var v = new Date(o[k] * 1000);
								o[k] = v.toStr('yyyy-MM-dd hh:mm:ss');
							} else if (o[k].length == 13) {
								var v = new Date(o[k] * 1000);
								o[k] = v.toStr('yyyy-MM-dd hh:mm:ss');
							}
						} else if (typeof(k) == 'number') {
							var v = new Date(o[k]);
							o[k] = v.toStr('yyyy-MM-dd hh:mm:ss');
						}
					}
				}
			}
		};

		/**
		 * 转为大写
		 * @param {String} word 单词
		 */
		Vue.config.globalProperties.$to_up = function(word) {
			return word.toUpperCase()
		};

		/**
		 * 显示小数位
		 * @param {Number} num 数值
		 * @param {Number} len 小数位
		 */
		Vue.config.globalProperties.$to_fixed = function(num, len = 4) {
			if (typeof(num) === 'number') {
				return num.toFixed(len);
			} else if (num) {
				var n = Number(num);
				return n.toFixed(len);
			} else {
				num = 0;
				return num.toFixed(len);
			}
		};

		/**
		 * 跳转地址
		 * @param {String} url
		 */
		Vue.config.globalProperties.$redirect = function(url) {
			if (url) {
				redirect_url = url;
			} else {
				return redirect_url;
			}
		};

		/**
		 * @description 转url字符串
		 * @param {Object} obj 被转换的对象
		 * @param {String} url 请求地址
		 * @return {String} url参数格式字符串
		 */
		Vue.config.globalProperties.$toUrl = function(obj, url) {
			var queryStr = "";
			for (var key in obj) {
				var value = obj[key];
				if (typeof(value) === 'number') {
					if (value > 0) {
						queryStr += "&" + key + "=" + obj[key];
					}
				} else if (value) {
					queryStr += "&" + key + "=" + encodeURI(value);
				}
			}
			if (url) {
				if (url.endWith('?') || url.endWith('&')) {
					return url + queryStr.replace('&', '');
				} else if (url.indexOf('?') === -1) {
					return url + queryStr.replace('&', '?');
				} else {
					return url + queryStr;
				}
			} else {
				return queryStr.replace('&', '');
			}
		};

		/**
		 * 转换名称
		 * @param {Array} list 数组
		 */
		Vue.config.globalProperties.$to_name = function(list, value, key = 'value', value_key = 'name') {
			var ret = "";
			for (var i = 0; i < list.length; i++) {
				var o = list[i];
				if (o[key] === value) {
					ret = o[value_key];
				}
			}
			return ret;
		};

		/**
		 * 补全请求url
		 * @param {String} url 现地址
		 * @return {String} 新地址
		 */
		Vue.config.globalProperties.$fullUrl = function(url) {
			var url_new = "";
			if (url) {
				if (url.indexOf("~/") === 0) {
					url_new = url.replace('~/', host);
				} else if (url.indexOf("/") === 0) {
					url_new = url.replace('/', host);
				} else {
					url_new = url;
				}
			}
			return url_new;
		};

		/**
		 * 补全请求url
		 * @param {String} url 现地址
		 * @return {String} 新地址
		 */
		Vue.config.globalProperties.$fullImgUrl = function(url) {
			if (url) {
				return this.$fullUrl(url);
			} else {
				return "/static/img/logo.png";
			}
		};

		/**
		 * 跳转导航
		 * @param {String} url 跳转地址
		 */
		Vue.config.globalProperties.$nav = function(url = 1) {
			if (typeof(url) == "string") {
				if (url.indexOf("//") === 0) {
					// 双斜杠跳转到标签页
					url = url.replace("//", "/");
					uni.switchTab({
						url
					});
					return
				} else if (url.indexOf("/") === 0) {
					// 单斜杠跳转到内页
					uni.navigateTo({
						url
					});
					return
				}
			}
			let pages = getCurrentPages(); // 获取当前页面栈
			if (pages.length === 1) {
				uni.switchTab({
					url: '/pages/root/index'
				});
			} else {
				uni.navigateBack({
					delta: 1
				});
			}
		};

		/**
		 * 检查是否有权获取位置
		 */
		Vue.config.globalProperties.$check_locationAuth = function() {
			return new Promise(function(resolve, reject) {
				uni.getSetting({
					success(res) {
						if (res.authSetting["scope.userLocation"]) {
							resolve(true);
						} else {
							resolve(false);
						}
					},
					fail() {
						resolve(null);
						reject("获取失败！");
					}
				})
			});
		};

		/**
		 * 请求位置授权
		 */
		Vue.config.globalProperties.$get_locationAuth = function() {
			return new Promise(function(resolve, reject) {
				uni.authorize({
					scope: "scope.userLocation",
					success(res) {
						resolve(true);
					},
					fail() {
						resolve(false);
						reject("拒绝了授权，无法获取周边信息！");
					}
				})
			});
		}

		/**
		 * 获取当前位置信息
		 */
		Vue.config.globalProperties.$get_location = function() {
			return new Promise(function(resolve, reject) {
				uni.getLocation({
					type: 'gcj20',
					success: function(res) {
						const latitude = res.latitude;
						const longitude = res.longitude;
						var url = 'http://apis.map.qq.com/ws/geocoder/v1/?location=' +
							latitude + ',' +
							longitude + '&key=QB6BZ-MKEW4-OIDUC-DAEES-K7MRH-W5FXB';
						uni.request({
							header: {
								"Content-Type": "application/text"
							},
							url,
							success(json) {
								resolve(json);
							},
							fail() {
								resolve(null);
								reject("获取位置失败！");
							}
						});
					}
				});
			});
		}

		/**
		 * 获取用户当前位置信息
		 */
		Vue.config.globalProperties.$get_map = async function() {
			// 检查是否有权获取用户位置
			var bl = await this.$check_locationAuth();
			if (!bl) {
				// 如果没有则申请获取位置权限
				bl = await this.$get_locationAuth();
				if (!bl) {
					return;
				}
			}
			return await this.$get_location();
		}

		/**
		 * 通过关键词获取指定位置信息
		 */
		Vue.config.globalProperties.$search_location = function(keyword) {
			return new Promise(function(resolve, reject) {
				var url = 'https://apis.map.qq.com/ws/geocoder/v1/?address=' + encodeURIComponent(keyword) +
					'&key=QB6BZ-MKEW4-OIDUC-DAEES-K7MRH-W5FXB';
				uni.request({
					header: {
						"Content-Type": "application/text"
					},
					url,
					success(json) {
						resolve(json);
					},
					fail() {
						resolve(null);
						reject("获取位置失败！");
					}
				});
			});
		};
		
		/**
		 * 过滤所有html标签
		 * @param {String} html
		 */
		Vue.config.globalProperties.$filter_html = function(html) {
			return html.replace(/<[^>]*>/g, '');
		}

		// 结束
		Vue.mixin({
			data() {
				return {
					phone_lock: 0,
					email_lock: 0,
					can_post: true
				}
			},
			methods: {
				/**
				 * 锁定
				 */
				$lock(type = "phone") {
					var key = type + "_lock";
					this[key] = 60;
					var timer = setInterval(() => {
						this[key] -= 1;
						if (this[key] <= 0) {
							clearInterval(timer)
							this[key] == 60
						}
					}, 1000)
				},
				/**
				 * 拨打电话
				 */
				$call_phone(tel, title = "拨号") {
					// #ifdef H5
					uni.showModal({
						title: title,
						content: tel,
						success: function(res) {
							if (res.confirm) {
								uni.makePhoneCall({
									phoneNumber: tel
								});
								this.$lock("phone");
							} else if (res.cancel) {

							}
						}
					});
					// #endif

					// #ifndef H5
					uni.makePhoneCall({
						phoneNumber: tel,
						// 成功回调
						success: (res) => {
							this.$lock("phone");
						},
						// 失败回调
						fail: (res) => {
							this.$call_phone(); //重复调用一次
						}
					});
					// #endif
				},
				/**
				 * 快速获取列表
				 * @param {String} key
				 * @param {String} url
				 */
				$getList(key, url, func) {
					var _this = this;
					this.$get(url, {}, (json) => {
						if (json.result) {
							_this[key] = json.result.list;
							if (func) {
								func(json.result);
							}
						}
					});
				},
				/**
				 * 快速获取对象
				 * @param {String} key
				 * @param {String} url
				 */
				$getObj(key, url, func) {
					var _this = this;
					this.$get(url, {}, (json) => {
						if (json.result) {
							_this[key] = json.result.obj;
							if (func) {
								func(json.result);
							}
						}
					});
				},
				/**
				 * 翻译
				 * @param {String} word 要翻译的单词
				 * @param {Object} func 回调函数
				 */
				$translate(word, func) {
					var _this = this;
					this.$get("~/api/service/translate?word=" + word, {}, function(json) {
						if (json.result) {
							func(json.result);
						} else if (json.error) {
							_this.$toast(json.error.message, 'danger');
						} else {
							_this.$toast('服务端连接错误！', 'danger');
						}
					});
				},
				/**
				 * 下载
				 * @param {String} url 下载地址
				 * @param {String} name 保存的名字
				 */
				$download(url, name) {
					var anchor = document.createElement('a');
					if (!name) {
						var arr = url.split("/");
						name = arr[arr.length - 1];
					}
					if ('download' in anchor) {
						anchor.href = url.replace("~/", host);
						anchor.setAttribute("download", name);
						anchor.className = "download-js-link";
						anchor.style.display = "none";
						document.body.appendChild(anchor);
						setTimeout(function() {
							anchor.click();
							document.body.removeChild(anchor);
						}, 66);
						return true;
					}
				},
				set_user(json) {
					if (json.result) {
						this.user.set(json.result);
					} else if (json.error) {
						// 非法访问或未登录
						this.user.sign_out();
					} else {
						this.$toast('服务器连接失败！');
					}
				},
				/**
				 * @description 获取用户信息
				 * @param {Function} func 回调函数
				 */
				async $get_user(func) {
					var _this = this;

					var token = uni.db.get("token");
					if (token) {
						// 存储token
						this.user.set({
							token
						});
					}

					if (func) {
						this.$get('~/api/user/state', null, function(json, status) {
							_this.set_user(json);
							if (func) {
								func(json.result);
							}
						});
					} else {
						var json = await this.$get('~/api/user/state');
						_this.set_user(json);
						return json.result;
					}
				},
				/**
				 * GET请求
				 * @param {String} url 请求地址
				 * @param {Object} body 请求参数
				 * @param {Funciton} func 回调函数，可以为空，为空则采用await返回值
				 * @return {Object} 返回请求结果
				 */
				async $get(url, body, func) {
					url = url.replace('~/', host);
					if (body) {
						url = this.$toUrl(body, url);
					}
					var token = uni.db.get('token');
					if (func) {
						uni.request({
							url, // 仅为示例，并非真实接口地址。
							method: "GET",
							header: {
								'x-auth-token': token
							},
							dataType: "json",
							success: function(res) {
								func(res.data);
							},
							error: function(err) {
								func(err);
							}
						});
					} else {
						var res = await uni.request({
							url, // 仅为示例，并非真实接口地址。
							header: {
								'x-auth-token': token
							},
							dataType: "json",
							data: body
						});
						return res.data;
					}
				},
				/**
				 * POST请求
				 * @param {String} url 请求地址
				 * @param {Object} data 请求参数
				 * @param {Funciton} func 回调函数，可以为空，为空则采用await返回值
				 * @return {Object} 返回请求结果
				 */
				async $post(url, data, func) {
					var token = uni.db.get('token');
					url = url.replace('~/', host);
					var _this = this;
					if (func) {
						uni.request({
							url, // 仅为示例，并非真实接口地址。
							method: "POST",
							header: {
								'x-auth-token': token,
								'Content-Type': 'application/json'
							},
							dataType: "json",
							data,
							success: function(res) {
								// if (i) {
								// 	_this.can_post = true;
								// }
								func(res.data);
							},
							error: function(err) {
								func(err);
							}
						});
					} else {
						var res = await uni.request({
							url, // 仅为示例，并非真实接口地址。
							method: "POST",
							header: {
								'x-auth-token': token,
								'Content-Type': 'application/json'
							},
							dataType: "json",
							data
						});
						return res.data;
					}
				},
				/**
				 * 上传文件
				 * @param {String} url 请求地址
				 * @param {Object} body 请求参数
				 * @param {Funciton} func 回调函数，可以为空，为空则采用await返回值
				 * @return {Object} 返回请求结果
				 */
				async $upload(url, body, func) {
					url = url.replace('~/', host);
					var token = uni.db.get('token');
					// 设置请求头 - 临时访问牌
					uni.setHeader('x-auth-token', token);
					// 设置请求头 - 发送的内容类型
					uni.setHeader('Content-Type', 'multipart/form-data', ['post']);
					if (func) {
						// 如果回调函数存在, 则采用异步
						uni.post(url, body).then((res) => {
							func(res.data);
						}).catch((res) => {
							func(res);
						});
					} else {
						// 否则采用同步
						var res = await uni.post(url, body);
						return res.data;
					}
				},
				/**
				 * 获取路径对应操作权限 鉴权
				 * @param {String} action 操作名
				 */
				$check_action(path, action = "get") {
					var o = this.$get_power(path);
					if (o) {
						console.log(o.user_group);
					}
					if (!o) {
						return true;
					}
					return o[action];
				},

				/**
				 * 获取用户组权限
				 * @param {String} user_group 用户组
				 */
				async $get_auth(user_group = "游客") {
					this.$get("~/api/sys/auth?", {
						user_group
					}, (json) => {
						if (json && json.result && json.result.list) {
							this.$store.commit("set_auth", json.result.list);
						}
					});
				},

				/**
				 * 获取权限 
				 * @param {String} path 路由路径
				 */
				$get_power(path) {
					var list = this.$store.state.web.auth;
					var obj;
					for (var i = 0; i < list.length; i++) {
						var o = list[i];
						if (o.path === path) {
							obj = o;
							break;
						}
					}
					return obj;
				},

				/**
				 * 是否有显示或操作字段的权限
				 * @param {String} action 操作名
				 * @param {String} field 查询的字段
				 */
				$check_field(action, field) {
					var o = this.$get_power(this.$route.path);
					var auth;
					if (o && o[action]) {
						auth = o[action]["field_" + action];
					}
					if (auth) {
						return auth.indexOf(field) !== -1;
					}
					return false;
				}
			}
		})
	}
}