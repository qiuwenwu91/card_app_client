class game_engine {
	/**
	 * 构造函数
	 * @param {Object} config
	 */
	constructor(config) {
		/**
		 * 配置参数
		 */
		this.config = Object.assign({}, config);
	}
}

/**
 * 更新（Update）
 */
game_engine.prototype.update = function() {}


/**
 * 渲染（Render）
 */
game_engine.prototype.render = function() {}

/**
 * 物理模拟（Physic）
 */
game_engine.prototype.physic = function() {}

/**
 * 输入处理（Input）
 */
game_engine.prototype.input = function() {}

/**
 * 镜头
 */
game_engine.prototype.camera = function() {

}

/**
 * 游戏循环
 */
game_engine.prototype.tick = function() {

}

/**
 * 运行游戏
 * 它负责更新游戏的所有逻辑和渲染操作。每次循环都会对游戏世界进行一次“Tick”，以确保所有的对象和事件都得到正确的更新。
 */
game_engine.prototype.run = function() {

}